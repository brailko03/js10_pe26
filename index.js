"use strict";

// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

// document.createElement(tag); elem.insertAdjacentHTML(where, html).
  

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

//  Cпочатку потрібно вибрати елемент для видалення, а потім просто застосувати метод remove().
//  const elementToRemove = document.querySelector(".navigation");
// elementToRemove.remove();

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
// append, prepend

// Практичні завдання

//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const link = document.createElement("a");
link.innerText = "Learn More"
link.href = "#";
const footer = document.querySelector("footer");
footer.append(link)

//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

const selectElement = document.createElement("select");
selectElement.id = "rating";
const main = document.querySelector("main");
main.prepend(selectElement);

//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
const optionElement = document.createElement("option");
optionElement.value = "4";
optionElement.innerText = "4 Stars"
selectElement.append(optionElement)

//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
const optionElement3 = document.createElement("option");
optionElement3.value = "3";
optionElement3.innerText = "3 Stars"
selectElement.append(optionElement3)
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
const optionElement2 = document.createElement("option");
optionElement2.value = "2";
optionElement2.innerText = "2 Stars"
selectElement.append(optionElement2)
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
const optionElement1 = document.createElement("option");
optionElement1.value = "1";
optionElement1.innerText = "1 Star"
selectElement.append(optionElement1)
//  
